const postModel = require("../../model/post");
const dbConn = require("../../lib/mongodb")();
describe("postModel ", () => {
    beforeAll(async (done) => {
        await dbConn
            .then(async (db) => {
                await db.collection("member")
                    .insertMany([
                        {
                            name: "testUser1",
                            userId: "testUser1",
                            passsword: "$2b$10$Sn5Hqaf6dwq6e8i9z5HOB.hNwPb6ls/umoyq/w0J7si6raNOsAcRS",
                            createdDate: new Date(),
                            following: []
                        },
                        {
                            name: "testUser2",
                            userId: "testUser2",
                            passsword: "$2b$10$Sn5Hqaf6dwq6e8i9z5HOB.hNwPb6ls/umoyq/w0J7si6raNOsAcRS",
                            createdDate: new Date(),
                            following: ["testUser1"]
                        },
                        {
                            name: "testUser3",
                            userId: "testUser3",
                            passsword: "$2b$10$Sn5Hqaf6dwq6e8i9z5HOB.hNwPb6ls/umoyq/w0J7si6raNOsAcRS",
                            createdDate: new Date(),
                            following: ["testUser1", "testUser2"]
                        }

                    ]);
                db.collection("post")
                    .insertMany([
                        {
                            title: "post1 by tu1",
                            text: "someTextHere",
                            tags: ["tag1", "tag2"],
                            viewCount: 0,
                            createdDate: new Date(),
                            isPrivate: false,
                            userId: "testUser1"
                        },
                        {
                            title: "post2 by tu1",
                            text: "someTextHere",
                            tags: ["tag1", "tag3"],
                            viewCount: 0,
                            createdDate: new Date(),
                            isPrivate: false,
                            userId: "testUser1"
                        },

                        {
                            title: "post3 by tu1",
                            text: "someTextHere",
                            tags: ["tag1", "tag3"],
                            viewCount: 0,
                            createdDate: new Date(),
                            isPrivate: true,
                            userId: "testUser1"
                        },
                        {
                            title: "post1 by tu2",
                            text: "someTextHere",
                            tags: ["tag1", "tag3"],
                            viewCount: 0,
                            createdDate: new Date(),
                            isPrivate: false,
                            userId: "testUser2"
                        },
                    ])

            });
    });

    test('create Post', async (done) => {
        await postModel.then(pm => {
            pm.create("post1 by tu3", "someText", "testUser3", true, ["tag5"])
                .then(result => {

                    expect(result.success).toBe(true);
                    done();
                })
        })
    });
    test('get posts', async (done) => {
        await postModel.then(pm => {
            pm.getPostsOfUser("testUser1", true)
                .then(result => {

                    expect(result.success).toBe(true);
                    expect(result.data.length == 3).toBe(true);
                    done();
                })
        })
    });
    test('dont get private posts', async (done) => {
        await postModel.then(pm => {
            pm.getPostsOfUser("testUser1", false)
                .then(result => {

                    expect(result.success).toBe(true);
                    expect(result.data.length == 2).toBe(true);
                    done();
                })
        })
    });
    test('get feed for testUser3', async (done) => {
        await postModel.then(pm => {
            pm.getFeed("testUser3")
                .then(result => {

                    expect(result.success).toBe(true);
                    expect(result.data.length == 3).toBe(true);
                    done();
                })
        })
    });
    afterAll(async (done) => {
        await dbConn
            .then(db => {
                return db.dropDatabase()
                    .finally(async () => {
                        await db.client.close(true);
                        return true;
                    })
            });
        await jest.resetModules();
        done()

    });

});