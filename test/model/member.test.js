const memberModel1 = require("../../model/member");
const dbConn = require("../../lib/mongodb")();
describe("memberModel ", () => {
    beforeAll(async (done) => {
        await dbConn
            .then(db => {
                db.collection("member")
                    .insertMany([
                        {
                            name: "testUser1",
                            userId: "testUser1",
                            passsword: "$2b$10$Sn5Hqaf6dwq6e8i9z5HOB.hNwPb6ls/umoyq/w0J7si6raNOsAcRS",
                            createdDate: new Date(),
                            following: []
                        },
                        {
                            name: "testUser2",
                            userId: "testUser2",
                            passsword: "$2b$10$Sn5Hqaf6dwq6e8i9z5HOB.hNwPb6ls/umoyq/w0J7si6raNOsAcRS",
                            createdDate: new Date(),
                            following: ["testUser1"]
                        },
                        {
                            name: "testUser3",
                            userId: "testUser3",
                            passsword: "$2b$10$Sn5Hqaf6dwq6e8i9z5HOB.hNwPb6ls/umoyq/w0J7si6raNOsAcRS",
                            createdDate: new Date(),
                            following: []
                        },

                    ])
                    .then(() => {
                        done();
                    })

            });
    });

    test('register writes db', async (done) => {
        await memberModel1.then(memberModel => {
            memberModel.register("tes3424t", "test User", "123456")
                .then(result => {
                    expect(result.success).toBe(true);
                    done();
                })
        })
    });
    test('login returns token', async (done) => {
        await memberModel1.then(memberModel => {
            memberModel.login("tes3424t", "123456")
                .then(result => {
                    expect(result.success).toEqual(true);
                    expect(result.data.token).toBeDefined();
                    done();
                })
        })
    });
    test('follow writes db', async (done) => {
        await memberModel1.then(mm => {
            mm.follow("testUser1", "testUser2")
                .then(res => {
                    dbConn
                        .then(db => {
                            db.collection("member")
                                .findOne({userId: "testUser1"})
                                .then(a => {

                                    expect(a.following.indexOf("testUser2")).not.toEqual(-1);
                                    done()
                                })
                        })
                })
        })

    });
    test('get followings', async (done) => {
        await memberModel1.then(mm => {
            mm.getFollowings("testUser1")
                .then(a => {
                    expect(a.data.filter(a => a.name == "testUser2").length).toEqual(1);
                    done();
                })
        })
    });
    test('get followers', async (done) => {
        await memberModel1.then(mm => {
            mm.getFollowers("testUser1")
                .then(a => {
                    expect(a.data.filter(a => a.name == "testUser2").length).toEqual(1);
                    done();
                })
        })
    });
    afterAll(async (done) => {
        await dbConn
            .then(db => {
                return db.dropDatabase()
                    .finally(async () => {
                        await db.client.close(true);
                        return true;
                    })
            });
        await jest.resetModules();
        done()

    });

});