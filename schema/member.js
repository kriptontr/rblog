const Joi = require('@hapi/joi');
Joi.objectId = require('joi-objectid')(Joi);
const memberSchema = Joi.object({
    _id: Joi.objectId(),
    userId: Joi.string().min(6).max(15).required(),
    password: Joi.string().min(6).max(25).required(),
    following: Joi.array().items(Joi.string()),
    createdDate: Joi.date().required(),
    name: Joi.string().max(255).required()
});
module.exports = memberSchema;