const Joi = require('@hapi/joi');
Joi.objectId = require('joi-objectid')(Joi);
const postSchema = Joi.object( {
    _id: Joi.objectId(),
    title: Joi.string().min(6).max(255).required(),
    userId: Joi.string().min(6).max(15).required(),
    text: Joi.string().required(),
    tags: Joi.array().items(Joi.string().min(3).max(32)).max(5),
    isPrivate: Joi.boolean(),
    createdDate: Joi.date().required(),
    viewCount: Joi.number()
});
module.exports = postSchema;