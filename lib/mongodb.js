
const MongoClient = require('mongodb').MongoClient;
const client = new MongoClient.connect(process.env.CONNSTR, {useNewUrlParser: true});
var _db = {};
var ret = function (dbName = (process.env.DBNAME || "testdb")) {
    if (!_db[dbName]) {

        _db[dbName] = client.then((mclient) => {
            _db[dbName] = mclient.db(dbName);
            _db[dbName].client = mclient;
            return _db[dbName];
        })
        return _db[dbName];
    } else {
        return (Promise.resolve(_db[dbName]))
    }
};

module.exports = ret ;
