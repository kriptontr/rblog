const jwt = require('jsonwebtoken');
const config = require("../config");
module.exports = function (req, res, next) {
    jwt.verify((req.headers["token"] || req.cookies["token"]), config.authSecret, function (err, decoded) {
        if (err) {
            res.send(401).end();
        } else {
            req.auth = decoded;
            next();
        }
    })
};