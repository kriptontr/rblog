var normalizedPath = require("path").join(__dirname, "");
global.model = {};
var ret = {};

var readyProms = require("fs").readdirSync(normalizedPath).map(function (file) {
    if (file === "index.js" || file.indexOf(".spec.") != -1 || file.indexOf(".test.") !== -1)
        return Promise.resolve();
    var moduleName = file.split(".")[0];
    return ((async () => {
        var module = require("./" + file);
        global.model[moduleName] = await module;
        ret[moduleName] = await module;
    })())

});
module.exports = Promise.all(readyProms).then(() => {
    return ret;
});