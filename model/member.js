const memberSchema = require("../schema/member");
const bcrypt = require('bcrypt');
let db = require("../lib/mongodb")();
const jwt = require('jsonwebtoken');
const config = require("../config");
require("../lib/Result.js");

const memberModel = db.then(db => {
    const memberCol = db.collection("member");
    let ret = {};
    /**
     *
     * @param userID
     * @param name
     * @param password
     * @returns {Promise<Result>}
     */
    ret.register = async function (userId, name, password) {
        let newMEmber = {name, userId, password, createdDate: new Date(), following: []};
        const {error, val} = memberSchema.validate(newMEmber);
        if (error) {
            return new Result(new Error(error))
        }
        let existingUser = await memberCol.findOne({userId});
        if (existingUser) {
            return new Result(new Error("User ID not available"))
        }
        newMEmber.password = await hashPwd(password);
        return memberCol.insertOne(newMEmber)
            .then(user => {
                return new Result({msg: "registered successfully"})
            })
            .catch(e => {
                return new Result(e);
            })
    };
    /**
     *
     * @param userId string
     * @param password string
     * @returns {Promise<Result>}
     */
    ret.login = async function (userId, password) {
        let member = await memberCol.findOne({userId});
        if (!member) {
            return new Result(new Error("Login Failed"))
        }
        const pwdPassed = await comparePwd(password, member.password);
        if (!pwdPassed) {
            return new Result(new Error("Login Failed"))
        }
        const token = await jwt.sign({_id: member._id, userId}, config.authSecret, {expiresIn: "10h"});
        return new Result({token})
    };
    ret.follow = async function (followerId, followingId) {
        return userExists(followingId).then(uExist => {
            if (uExist) {
                return memberCol.update({userId: followerId}, {$addToSet: {following: followingId}})
                    .then(a => {
                        return new Result({message: "ok"})
                    })
                    .catch(e => {
                        return new Result(e)
                    })
            } else {
                return new Result(new Error("User not exist"))
            }
        })

    };
    ret.unFollow = async function (followerId, followingId) {
        return userExists(followingId).then(uExist => {
            if (uExist) {
                return memberCol.update({userId: followerId}, {$pull: {following: {$in: [followingId]}}})
                    .then(a => {
                        if (a.result.nModified > 0) {
                            return new Result({message: "ok"})
                        } else {
                            return new Result(new Error("user not fount / not following"))
                        }

                    })
                    .catch(e => {
                        return new Result(e)
                    })
            } else {
                return new Result(new Error("User not exist"))
            }
        })

    };
    ret.getFollowings = async function (userId, skip = 0, limit = 20) {
        return memberCol.aggregate([
            {
                '$match': {
                    'userId': userId
                }
            }, {
                '$project': {
                    'following': 1,
                    '_id': 0
                }
            }, {
                '$unwind': {
                    'path': '$following'
                }
            }, {
                '$lookup': {
                    'from': 'member',
                    'localField': 'following',
                    'foreignField': 'userId',
                    'as': 'member'
                }
            }, {
                '$unwind': {
                    'path': '$member',
                    'preserveNullAndEmptyArrays': true
                }
            }, {
                '$group': {
                    '_id': '$member._id',
                    'name': {
                        '$first': '$member.name'
                    },
                    'userId': {
                        '$first': '$member.userId'
                    }
                }
            }, {
                '$project': {
                    'name': 1,
                    'userId': 1,
                    '_id': 0
                }
            }
        ])
            .skip(skip).limit(limit).toArray()
            .then(a => {
                return new Result(a);
            })
            .catch(e => {
                return new Result(e);
            })
    };
    ret.getFollowers = async function (userId, skip = 0, limit = 20) {
        return memberCol.find({following: {$elemMatch: {$all: [userId]}}})
            .project({name: 1, userId: 1})
            .skip(skip).limit(limit).toArray()
            .then(a => {
                return new Result(a);
            })
            .catch(e => {
                return new Result(e);
            })
    };
    ret.returnAll = async function (skip = 0, limit = 20) {
        return memberCol.find().project({userId: 1, name: 1})
            .skip(skip).limit(limit).toArray()
            .then(a => {
                return new Result(a)
            })
            .catch(e => {
                return new Result(e)
            })
    };
    async function userExists(userId) {
        return memberCol.findOne({userId})
            .then(a => {
                return !!a;
            })
    }

    return ret;
});

async function hashPwd(pwd) { //promisifiying hashPWD
    return new Promise((resolve, reject) => {
        bcrypt.hash(pwd, 10, function (err, hash) {
            if (err) {
                reject(err)
            }
            resolve(hash)
        });
    })
}

async function comparePwd(pwd, hash) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(pwd, hash, function (err, res) {
            if (err) {
                reject(err)
            }
            resolve(res)
        });
    })

}

module.exports = memberModel;
//TODO divide this model into files