const postSchema = require("../schema/post");
let db = require("../lib/mongodb")();
require("../lib/Result.js");
const memberModel = require("./member")
const postModel = db.then(db => {
    const postCol = db.collection("post");
    let ret = {};
    ret.create = async function (title, text, userId, isPrivate = false, tags) {
        let newPost = {
            title, text, isPrivate, userId,
            tags: (tags instanceof Array) ? tags : [],
            viewCount: 0,
            createdDate: new Date()
        };
        const {error, val} = postSchema.validate(newPost);
        if (error) {
            return new Result(new Error(error))
        }
        return postCol.insert(newPost)
            .then(a => {
                return new Result({message: "ok", id: a.insertedIds[0]})
            })
    };
    ret.getPostsOfUser = async function (userId, showPrivate = false, skip = 0, limit = 20) {
        return postCol.find(showPrivate ? {userId} : {userId, isPrivate: false}).skip(skip).limit(limit).toArray()
            .then(a => {

                incViewCount(a.map(a => a._id));

                return new Result(a)
            })
            .catch(e => {
                return new Result(e)
            })
        // TODO refactor all those result returns with a decorator
    };
    ret.getFeed = async function (userId, skip = 0, limit = 25) {
        return memberModel.then(mm => {
            return mm.getFollowings(userId, 0, 9999999)
                .then(result => {
                    if (result.success) {
                        return postCol.find({
                            userId: {$in: result.data.map(a => a.userId)},
                            isPrivate: false
                        }, {isPrivate: 0})
                            .skip(skip).limit(limit)
                            .toArray().then(data => {
                                incViewCount(data.map(a => a._id));
                                return new Result(data);
                            })
                    } else {
                        return result;
                    }

                })

        })
            .catch(e => {
                return new Result(e);
            })
    };

    async function incViewCount(postIdArr) {
        return postCol.update({_id: {$in: postIdArr}}, {$inc: {viewCount: 1}}, {multi: true})
    }

    return ret;
});


module.exports = postModel;