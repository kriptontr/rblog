const express = require('express');
//var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
const app = express();
var cors = require('cors')
app.use(express.json());
app.use(cookieParser());
//app.use(cors())
require("./model")
    .then(() => {
        app.use("/api", require("./api/index"))
        app.use(express.static('ui-dist'))
    })
    .then(() => {
        app.use(function (err, req, res, next) {
            console.error(err.stack)
            res.send(new Result(err))
        })
    })
    .then(() => {
        app.listen(process.env.PORT || 3001, () => console.log(`Example app listening on port ${process.env.PORT}!`))
    });



