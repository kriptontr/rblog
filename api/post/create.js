function createHandler(req, res) {
    model.post.create(req.body.title, req.body.text, req.auth.userId, req.body.isPrivate, req.body.tags)
        .then(Result => {
            res.json(Result);
        })
}

module.exports = createHandler;