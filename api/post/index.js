const router = require('express').Router();

const auth = require("../../lib/auth");
const {get4others, get4Self} = require("./get");
router.post("/", auth, require("./create"));
router.get("/feed", auth, require("./feed"));
router.get("/:userId", get4others);

router.get("/", auth, get4Self);

module.exports = router;