function get4Self(req, res) {
    model.post.getPostsOfUser(req.auth.userId, true, parseInt(req.query.skip) || 0, parseInt(req.query.limit) || 25)
        .then(Result => {
            res.json(Result);
        })
}

function get4others(req, res) {
    model.post.getPostsOfUser(req.params.userId, false, parseInt(req.query.skip) || 0, parseInt(req.query.limit) || 25)
        .then(Result => {
            res.json(Result);
        })
}

module.exports = {get4others, get4Self};