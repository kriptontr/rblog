function feed(req, res) {
    model.post.getFeed(req.auth.userId, parseInt(req.query.skip) || 0, parseInt(req.query.limit) || 25)
        .then(Result => {
            res.json(Result);
        })
}

module.exports = feed;