const router = require('express').Router();

router.use("/member", require("./member/index"));
router.use("/post", require("./post/index"));


module.exports = router;