

function followHandler(req, res) {
    model.member.follow(req.auth.userId, req.params.userId)
                .then(Result => {
                    res.json(Result);
                })

}

function unFollowHandler(req, res) {
    model.member.unFollow(req.auth.userId, req.params.userId)
                .then(Result => {
                    res.json(Result);
                })

}

function getFollowersHandler(req, res) {
    model.member.getFollowers(req.auth.userId || req.params.userId, parseInt(req.query.skip) || 0, parseInt(req.query.limit) || 25)
                .then(Result => {
                    res.json(Result);
                })

}

function getFollowingsHandler(req, res) {
    model.member.getFollowings(req.auth.userId || req.params.userId, parseInt(req.query.skip) || 0, parseInt(req.query.limit) || 25)
                .then(Result => {
                    res.json(Result);
                })


}

const ret = {getFollowersHandler, getFollowingsHandler, unFollowHandler, followHandler}
module.exports = ret;