const router = require('express').Router();
const {getFollowersHandler, getFollowingsHandler, unFollowHandler, followHandler} = require("./follow");
const auth = require("../../lib/auth");

router.post("/login/", require("./login"));
router.post("/", require("./register"));
router.post("/follow/:userId", auth, followHandler);
router.get("/follow", auth, getFollowersHandler);
router.delete("/follow/:userId", auth, unFollowHandler);
router.get("/following", auth, getFollowingsHandler);
router.get("/", require("./all"));

module.exports = router;