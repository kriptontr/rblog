function getAll(req, res) {

    model.member.returnAll(parseInt(req.query.skip) || 0, parseInt(req.query.limit) || 25)
        .then(Result => {
            res.json(Result);
        })

}

module.exports = getAll;